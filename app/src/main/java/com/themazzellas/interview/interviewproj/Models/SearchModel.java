package com.themazzellas.interview.interviewproj.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class SearchModel implements Parcelable {

    @Override
    public void writeToParcel(Parcel out, int flags) {

    }

    private SearchModel(Parcel in) {

    }

    public static final Parcelable.Creator<SearchModel> CREATOR = new Parcelable.Creator<SearchModel>() {
        @Override
        public SearchModel createFromParcel(Parcel in) {
            return new SearchModel(in);
        }

        @Override
        public SearchModel[] newArray(int size) {
            return new SearchModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
}
