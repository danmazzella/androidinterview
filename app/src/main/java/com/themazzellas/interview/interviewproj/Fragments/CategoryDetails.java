package com.themazzellas.interview.interviewproj.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.themazzellas.interview.interviewproj.Models.SearchModel;
import com.themazzellas.interview.interviewproj.R;

public class CategoryDetails extends Fragment {
    private Context context;

    public static CategoryDetails newInstance(SearchModel searchItem) {
        CategoryDetails categoryDetails = new CategoryDetails();
        Bundle fragData = new Bundle();
        fragData.putParcelable("searchItem", searchItem);
        categoryDetails.setArguments(fragData);

        return categoryDetails;
    }

    @Override
    public void onAttach(Context _context) {
        super.onAttach(_context);
        context = _context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.category_details, container, false);

        // TODO: Update view to show data that was passed in from activity

        return view;
    }
}
