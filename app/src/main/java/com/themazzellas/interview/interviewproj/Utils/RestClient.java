package com.themazzellas.interview.interviewproj.Utils;

import com.themazzellas.interview.interviewproj.API.SearchApi;

import java.util.Collections;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static SearchApi searchApi;

    private static Retrofit restAdapter;

    public RestClient() {
    }

    private static void setupRestAdapter() {
        String ROOT_URL = "https://api.staging.taskhuman.com";

        ConnectionSpec spec = new
                ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectionSpecs(Collections.singletonList(spec))
                .build();

        restAdapter = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static SearchApi getSearchApi() {
        if (restAdapter == null) {
            setupRestAdapter();
        }

        if (searchApi == null) {
            searchApi = restAdapter.create(SearchApi.class);
        }

        return searchApi;
    }
}