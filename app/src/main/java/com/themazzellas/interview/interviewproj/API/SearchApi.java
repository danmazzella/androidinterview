package com.themazzellas.interview.interviewproj.API;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SearchApi {
    @POST("/v0.2/discoverySearch/Search")
    Call<JsonObject> search(@Header("Authorization") String apiKey, @Query("searchText") String query);
}
