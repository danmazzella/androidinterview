package com.themazzellas.interview.interviewproj.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.themazzellas.interview.interviewproj.Models.SearchModel;
import com.themazzellas.interview.interviewproj.R;

import java.util.ArrayList;

// TODO: When an item is clicked, the Activity should add "CategoryDetails" fragment

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
    private ArrayList<SearchModel> searchArrayList;

    public SearchAdapter(ArrayList<SearchModel> searchArrayList) {
        this.searchArrayList = searchArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        // Update holder views with data
    }

    @Override
    public int getItemCount() {
        return searchArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View view) {
            super(view);

        }
    }
}